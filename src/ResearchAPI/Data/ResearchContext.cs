﻿using MongoDB.Driver;
using ResearchAPI.Entities;
using ResearchAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResearchAPI.Data
{
    public class ResearchContext
    {
        public ResearchContext(IResearchDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            Researches = database.GetCollection<Research>(settings.CollectionName);
            ResearchContextSeed.SeedData(Researches);
        }

        public IMongoCollection<Research> Researches{ get; }

    }
}

﻿using MongoDB.Driver;
using ResearchAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResearchAPI.Data
{
    public class ResearchContextSeed
    {
        public static void SeedData(IMongoCollection<Research> researchCollection)
        {
            bool existsResearches= researchCollection.Find(p => true).Any();
            if (!existsResearches)
            {
                researchCollection.InsertManyAsync(GetPreconfiguredResearches());
            }
        }

        private static IEnumerable<Research> GetPreconfiguredResearches()
        {
            return new List<Research>();
        }



    }
}

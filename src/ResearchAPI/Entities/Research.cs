﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResearchAPI.Entities
{
    public class Research
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<Canvas> Canvasess{ get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<Post> Posts { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ModeratorId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> Members { get; set; }
    }
}

﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResearchAPI.Entities
{
    public class Post
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Content { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<Comment> Comments { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ResearchId { get; set; }
    }
}
